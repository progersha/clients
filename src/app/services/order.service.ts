import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {OrderInterface} from '../models/order.interface';
import {LocalStorageService} from './local-storage.service';
import {ResponseInterface} from '../models/response.interface';

@Injectable({
    providedIn: 'root'
})
export class OrderService {

    private orders: OrderInterface[];
    private key = 'orders';

    constructor(private localStorageService: LocalStorageService) {
        this.fetch();
    }

    private fetch() {
        this.localStorageService.get(this.key).subscribe(orders => {
            this.orders = JSON.parse(orders);
        });
    }

    addOrder(clientId: number, serviceId: number): Observable<ResponseInterface> {
        const order = <OrderInterface> {
            id: this.orders.length,
            clientId: clientId,
            serviceId: serviceId
        };
        return this.localStorageService.put(this.key, this.orders, order);
    }

    getOrders(): Observable<OrderInterface[]> {
        return of(this.orders);
    }

    getClientById(id: number): Observable<OrderInterface> {
        return of(this.orders.find(client => client.id === +id));
    }

    getOrdersByClientId(id: number): Observable<OrderInterface[]> {
        return of(this.orders.filter(order => order.clientId === +id));
    }
}
