import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {CLIENTS, ORDERS, SERVICES} from '../models/mock';
import {ResponseInterface} from '../models/response.interface';

@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {

    constructor() {
        this.init();
    }

    get(key: string): Observable<any> {
        return of(localStorage.getItem(key));
    }

    set(key: string, value: string) {
        localStorage.setItem(key, value);
    }

    put(key: string, list: Array<any>, body: any): Observable<ResponseInterface> {
        list.push(body);
        this.set(key, JSON.stringify(list));
        return of({
            id: body.id,
            status: 'success'
        });
    }

    init() {
        if (!localStorage.getItem('clients')) {
            this.set('clients', JSON.stringify(CLIENTS));
        }
        if (!localStorage.getItem('services')) {
            this.set('services', JSON.stringify(SERVICES));
        }
        if (!localStorage.getItem('orders')) {
            this.set('orders', JSON.stringify(ORDERS));
        }
    }
}
