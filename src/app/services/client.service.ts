import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {ClientInterface} from '../models/client.interface';
import {CLIENTS, SERVICES, ORDERS} from '../models/mock';
import {OrderInterface} from '../models/order.interface';
import {ServiceInterface} from '../models/service.interface';
import {LocalStorageService} from './local-storage.service';
import {ResponseInterface} from '../models/response.interface';

@Injectable({
    providedIn: 'root'
})
export class ClientService {

    private clients: ClientInterface[];
    private key = 'clients';

    constructor(private localStorageService: LocalStorageService) {
        this.fetch();
    }

    private fetch() {
        this.localStorageService.get(this.key).subscribe(clients => {
            this.clients = JSON.parse(clients);
        });
    }

    addClient(clientName: string): Observable<ResponseInterface> {
        const client = {
            id: this.clients.length,
            name: clientName
        };
        return this.localStorageService.put(this.key, this.clients, client);
    }

    getClients(): Observable<ClientInterface[]> {
        return of(this.clients);
    }

    getClientById(id: number): Observable<ClientInterface> {
        return of(this.clients.find(client => client.id === +id));
    }

    update(client: ClientInterface) {
        this.clients = this.clients.map(item => {
            if (item.id === client.id) {
                item = client;
            }
            return item;
        });
        this.localStorageService.set(this.key, JSON.stringify(this.clients));
    }
}
