import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {ServiceInterface} from '../models/service.interface';
import {LocalStorageService} from './local-storage.service';
import {ResponseInterface} from '../models/response.interface';

@Injectable({
    providedIn: 'root'
})
export class ServiceService {
    services: ServiceInterface[];
    key = 'services';

    constructor(private localStorageService: LocalStorageService) {
        this.fetch();
    }

    private fetch() {
        this.localStorageService.get(this.key).subscribe(services => {
            this.services = JSON.parse(services);
        });
    }

    addService(serviceName: string, servicePrice: number): Observable<ResponseInterface> {
        const service = {
            id: this.services.length,
            name: serviceName,
            price: servicePrice
        };
        return this.localStorageService.put(this.key, this.services, service);
    }

    getServices(): Observable<ServiceInterface[]> {
        return of(this.services);
    }

    getServiceById(id: number): Observable<ServiceInterface> {
        return of(this.services.find(service => service.id === +id));
    }

}
