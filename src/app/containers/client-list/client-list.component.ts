import {Component, OnInit} from '@angular/core';
import {ClientService} from '../../services/client.service';
import {ClientInterface} from '../../models/client.interface';
import {Router} from '@angular/router';

@Component({
    selector: 'app-client-list',
    templateUrl: './client-list.component.html',
    styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {

    clients: ClientInterface[];
    clientName: string;

    constructor(private clientService: ClientService,
                private router: Router) {
    }

    ngOnInit() {
        this.clientService.getClients().subscribe(clients => {
            this.clients = clients;
        });
    }

    onSubmit() {
        this.clientService.addClient(this.clientName).subscribe((clientId) => {
            console.log(clientId, this.clientName);
            this.clientName = '';
        });
    }

}
