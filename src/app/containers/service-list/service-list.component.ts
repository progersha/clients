import {Component, OnInit} from '@angular/core';
import {ServiceInterface} from '../../models/service.interface';
import {ServiceService} from '../../services/service.service';
import {Router} from '@angular/router';
import {ResponseInterface} from '../../models/response.interface';

@Component({
    selector: 'app-service-list',
    templateUrl: './service-list.component.html',
    styleUrls: ['./service-list.component.scss']
})
export class ServiceListComponent implements OnInit {

    services: ServiceInterface[] = [];
    serviceName: string;
    servicePrice: number;

    constructor(private serviceService: ServiceService,
                private router: Router) {
    }

    ngOnInit() {
        this.serviceService.getServices().subscribe(services => {
            this.services = services;
        });
    }

    onSubmit() {
        this.serviceService.addService(this.serviceName, this.servicePrice).subscribe((resp: ResponseInterface) => {
            this.serviceName = '';
            this.servicePrice = null;
        });
    }


}
