import {Component, OnInit} from '@angular/core';
import {ClientService} from '../../services/client.service';
import {ClientInterface} from '../../models/client.interface';
import {ActivatedRoute} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {OrderInterface} from '../../models/order.interface';
import {ServiceService} from '../../services/service.service';
import {OrderService} from '../../services/order.service';
import {OrderDetail} from '../../models/order-detail.model';
import {ServiceInterface} from '../../models/service.interface';
import {ResponseInterface} from '../../models/response.interface';

@Component({
    selector: 'app-client-item',
    templateUrl: './client-item.component.html',
    styleUrls: ['./client-item.component.scss']
})
export class ClientItemComponent implements OnInit {

    client: ClientInterface;
    orders: OrderInterface[];
    services: ServiceInterface[];
    selectedService: ServiceInterface;
    orderDetailList: OrderDetail[] = [];
    isLoad = false;
    serviceId: number;
    payment: string;
    orderSum = 0;
    payoutPercent = 0;

    constructor(private clientService: ClientService,
                private serviceService: ServiceService,
                private orderService: OrderService,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.route.params.pipe(
            switchMap((params) => {
                return this.clientService.getClientById(params['id']);
            }),
            switchMap((client) => {
                this.client = client;
                return this.orderService.getOrdersByClientId(this.client.id);
            }),
        ).subscribe(orders => {
            this.orders = orders;
            this.services = this.serviceService.services;
            this.orders.forEach(item => {
                this.setDetail(item);
            });
            this.updatePayment();
            this.isLoad = true;
        });
    }

    addBalance() {
        if (+this.payment > 0) {
            this.client.balance += +this.payment;
            this.payment = '';
            this.updatePayment();
            this.clientService.update(this.client);
        }
    }

    setDetail(order: OrderInterface) {
        const orderDetail = new OrderDetail();
        orderDetail.id = order.id;
        orderDetail.service = this.services.find(it => it.id === order.serviceId);
        this.orderDetailList.push(orderDetail);
        this.updatePayment();
    }

    onChange(service) {
        this.serviceId = service;
    }

    addOrder() {
        this.orderService.addOrder(this.client.id, +this.serviceId).subscribe((resp: ResponseInterface) => {
            this.setDetail({
                id: +resp.id,
                clientId: +this.client.id,
                serviceId: +this.serviceId
            });
        });
    }

    updatePayment() {
        this.orderSum = 0;
        this.orderDetailList.forEach(order => {
            this.orderSum += order.service.price;
        });
        const percent = !this.client.balance ? 0 : this.client.balance / this.orderSum;
        this.payoutPercent = !this.client.balance ? 0 : Math.ceil(percent * 100);
        this.orderDetailList = this.orderDetailList.map(order => {
            order.payment = +(order.service.price * percent).toFixed(2);
            return order;
        });
        if (!this.client.debt) {
            this.client.debt = 0;
        }
        if (!this.client.balance) {
            this.client.balance = 0;
        }
        this.client.debt = this.orderSum - this.client.balance;
    }
}
