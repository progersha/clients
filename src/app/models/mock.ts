import {ClientInterface} from './client.interface';
import {OrderInterface} from './order.interface';

export const CLIENTS: ClientInterface[] = [
    {
        id: 0,
        name: 'Medicroix',
        debt: 0,
        balance: 0,
    },
    {
        id: 1,
        name: 'Xelegyl',
        debt: 4500,
        balance: 1000,
    },
    {
        id: 2,
        name: 'Nexgene',
        debt: 0,
        balance: 2000,
    },
    {
        id: 3,
        name: 'Boilcat',
        debt: 2700,
        balance: 2300,
    },
    {
        id: 4,
        name: 'Zaj',
        debt: 0,
        balance: 0,
    },
    {
        id: 5,
        name: 'Centregy',
        debt: 0,
        balance: 0,
    }
];

export const SERVICES = [
    {
        id: 0,
        name: 'Equitox',
        price: 1000
    },
    {
        id: 1,
        name: 'Xanide',
        price: 2000
    },
    {
        id: 2,
        name: 'Gadtron',
        price: 1500
    },
    {
        id: 3,
        name: 'Digique',
        price: 3000
    },
    {
        id: 4,
        name: 'Exoteric',
        price: 5000
    }
];

export const ORDERS: OrderInterface[] = [
    {
        id: 0,
        clientId: 1,
        serviceId: 1
    },
    {
        id: 1,
        clientId: 1,
        serviceId: 1
    }, {
        id: 2,
        clientId: 2,
        serviceId: 1
    }, {
        id: 3,
        clientId: 1,
        serviceId: 2
    }, {
        id: 4,
        clientId: 3,
        serviceId: 4
    }
];
