export interface ServiceInterface {
    id: number;
    name: string;
    price: number;
}
