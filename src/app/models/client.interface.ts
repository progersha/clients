export interface ClientInterface {
    id: number;
    name: string;
    balance?: number;
    debt?: number;
}
