import {ServiceInterface} from './service.interface';

export class OrderDetail {
    id: number;
    service: ServiceInterface;
    payment = 0;
}
