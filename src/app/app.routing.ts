import {Routes} from '@angular/router/src/config';
import {ClientListComponent} from './containers/client-list/client-list.component';
import {ClientItemComponent} from './containers/client-item/client-item.component';
import {ServiceListComponent} from './containers/service-list/service-list.component';

export const APP_ROUTE: Routes = [
    {
        path: '',
        component: ClientListComponent
    }, {
        path: 'clients',
        redirectTo: '',
    }, {
        path: 'clients/:id',
        component: ClientItemComponent
    }, {
        path: 'services',
        component: ServiceListComponent
    }
];
