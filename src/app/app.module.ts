import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {ClientListComponent} from './containers/client-list/client-list.component';
import {RouterModule} from '@angular/router';
import {APP_ROUTE} from './app.routing';
import {ClientItemComponent} from './containers/client-item/client-item.component';
import {FormsModule} from '@angular/forms';
import {ServiceListComponent} from './containers/service-list/service-list.component';

@NgModule({
    declarations: [
        AppComponent,
        ClientListComponent,
        ClientItemComponent,
        ServiceListComponent,
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(APP_ROUTE),
        FormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
